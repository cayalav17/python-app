FROM python:3.9.1
ADD . /python-app
WORKDIR /python-app
RUN pip install -r requirements.txt